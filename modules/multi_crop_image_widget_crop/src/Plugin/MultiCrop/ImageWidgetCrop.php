<?php

namespace Drupal\multi_crop_image_widget_crop\Plugin\MultiCrop;

use Drupal\image\Entity\ImageStyle;
use Drupal\multi_crop\MultiCropPluginBase;

/**
 * Plugin implementation of the multi_crop.
 *
 * @MultiCrop(
 *   id = "image_widget_crop",
 *   target_field_name = "image_crop",
 *   label = @Translation("Plugin image_widget_crop"),
 *   description = @Translation("Manage Multicrop for image_widget_crop.")
 * )
 */
class ImageWidgetCrop extends MultiCropPluginBase {

  /**
   * {@inheritdoc}
   */
  public function saveCrop($crop_settings, string $image_style, string $old_uri, string $new_uri, int $width, int $height) {

    $style = ImageStyle::load($image_style);

    $style_dependencies = $style->getDependencies();
    foreach ($style_dependencies as $dep) {
      if (strstr($dep[0], 'crop.type')) {
        $croptype = str_replace('crop.type.', '', $dep[0]);
      }
    }

    $crop_created = FALSE;
    // Filter to the concerned crop setting.
    foreach ($crop_settings['crop_wrapper'] as $crop_type => $container) {
      // Get Crop value.
      if ($croptype != $crop_type) {
        continue;
      }

      $crop_values = $container['crop_container']['values'];

      // If crop are set.
      if ($crop_values['crop_applied'] == '1') {

        // Recover Crop.
        $crop = $this->retrievCrop($crop_type, $old_uri, $new_uri);

        // Get Center coordinate of crop zone on original image.
        $axis_coordinate = \Drupal::service('image_widget_crop.manager')->getAxisCoordinates(
          ['x' => $crop_values['x'], 'y' => $crop_values['y']],
          ['width' => $crop_values['width'], 'height' => $crop_values['height']]
        );

        // Set position & size.
        $crop->setPosition($axis_coordinate['x'], $axis_coordinate['y']);
        $crop->setSize($crop_values['width'], $crop_values['height']);

        // Save Crop.
        $crop->save();
        $crop_created = TRUE;
      }
    }

    return $crop_created;
  }

  /**
   * {@inheritdoc}
   */
  public static function processFieldData($field_data) {

    // Filter to the concerned crop setting.
    foreach ($field_data['crop_wrapper'] as $crop_type => $container) {
      // Get Crop value.
      $crop_values = $container['crop_container']['values'];
      if ($crop_values['crop_applied'] == 0) {
        unset($field_data['crop_wrapper'][$crop_type]);
      }
    }

    if (count($field_data['crop_wrapper']) == 0) {
      return NULL;
    }
    else {
      return $field_data;
    }

  }

}
