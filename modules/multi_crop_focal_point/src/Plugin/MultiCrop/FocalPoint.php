<?php

namespace Drupal\multi_crop_focal_point\Plugin\MultiCrop;

use Drupal\multi_crop\MultiCropPluginBase;

/**
 * Plugin implementation of the multi_crop.
 *
 * @MultiCrop(
 *   id = "focal_point",
 *   target_field_name = "focal_point",
 *   label = @Translation("Plugin Focal Point"),
 *   description = @Translation("Manage Multicrop for Focal Point Crop.")
 * )
 */
class FocalPoint extends MultiCropPluginBase {

  /**
   * {@inheritdoc}
   */
  public function saveCrop($crop_settings, string $image_style, string $old_uri, string $new_uri, int $width, int $height) {

    // Get Crop type.
    $crop_type = \Drupal::config('focal_point.settings')->get('crop_type');

    // Recover Crop.
    $crop = $this->retrievCrop($crop_type, $old_uri, $new_uri);

    // Recalculate coordinate.
    [$x, $y] = explode(',', $crop_settings);
    $absolute = \Drupal::service('focal_point.manager')->relativeToAbsolute((float) $x, (float) $y, $width, $height);

    // Set anchor.
    $anchor = $crop->anchor();
    if ($anchor['x'] != $absolute['x'] || $anchor['y'] != $absolute['y']) {
      $crop->setPosition($absolute['x'], $absolute['y']);
      $crop->save();

    }

    return TRUE;
  }

}
