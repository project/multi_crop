<?php

/**
 * @file
 * Contains multicrop implemented hooks.
 */

module_load_include('field.inc', 'image');

/**
 * Implements hook media_library_media_modify_referenced_entity_values_alter().
 */
function multi_crop_media_library_media_modify_referenced_entity_values_alter(&$values, $fields, $referenced_entity) {

  // Load multi crop plugins.
  $multi_crop_plugins = \Drupal::service('plugin.manager.multi_crop');
  $plugin_definitions = $multi_crop_plugins->getDefinitions();

  // Parse all entity field.
  foreach ($fields as $field_name) {

    // Pass on each field values.
    $item_list = $referenced_entity->get($field_name);
    foreach ($item_list->getValue() as $key => $item) {
      // If a field as a target item of one of plugins.
      foreach ($plugin_definitions as $plugin) {

        $target_field = $plugin['target_field_name'];
        if (isset($item[$target_field])) {

          $data = $plugin['class']::processFieldData($item[$target_field]);
          if($data != null) {
            $values[$field_name][$key][$target_field] = $data;
          }
          else {
            unset( $values[$field_name][$key][$target_field] );
          }
        }

      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function multi_crop_theme() {

  return [
    // Override core image_formatter theme.
    'multi_crop_image_formatter' => [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'url' => NULL,
        'image_style' => NULL,
        'multi_crop_attributes' => [],
      ],
    ],
  ];
}

/**
 * Override core image_formatter theme.
 *
 * Default template: multi-crop-image-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 *   - multi_crop_attributes: An array with crop override
 */
function template_preprocess_multi_crop_image_formatter(array &$variables) {

  // Run core image formatter.
  template_preprocess_image_formatter($variables);

  if (isset($variables['multi_crop_attributes'])) {

    // Prepare Crop & generate new image path
    $new_uri = \Drupal::service('multi_crop.service')->prepareCropImage($variables['image'], $variables['multi_crop_attributes']);
    $variables['image']['#uri'] = $new_uri;
  
  }
}
