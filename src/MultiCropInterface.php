<?php

namespace Drupal\multi_crop;

/**
 * Interface for multi_crop plugins.
 */
interface MultiCropInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the Target Field of the plugin.
   *
   * @return string
   *   Target field name.
   */
  public function getTargetFieldName();

  /**
   * Save overided crop.
   *
   * @param $crop_settings
   *   Overwritten crop data.
   * @param string $image_style_name
   *   target Image Style.
   * @param string $old_uri
   *   Uri of original image.
   * @param string $new_uri
   *   Uri of the "copy image".
   * @param int $width
   *   Source Image Width.
   * @param int $height
   *   Source Image Height.
   *
   * @return bool
   *   If a custom crop can be applied.
   */
  public function saveCrop($crop_settings, string $image_style_name, string $old_uri, string $new_uri, int $width, int $height);

  /**
   * Process Field data to save them.
   *
   * @param array $field_data
   *   Field data.
   *
   * @return mixed
   *   Data to save in overides.
   */
  public static function processFieldData(array $field_data);

}
