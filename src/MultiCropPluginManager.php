<?php

namespace Drupal\multi_crop;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * MultiCrop plugin manager.
 */
class MultiCropPluginManager extends DefaultPluginManager {

  /**
   * Constructs MultiCropPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/MultiCrop',
      $namespaces,
      $module_handler,
      'Drupal\multi_crop\MultiCropInterface',
      'Drupal\multi_crop\Annotation\MultiCrop'
    );
    $this->alterInfo('multi_crop_info');
    $this->setCacheBackend($cache_backend, 'multi_crop_plugins');
  }

}
