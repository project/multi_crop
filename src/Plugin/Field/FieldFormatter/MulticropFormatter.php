<?php

namespace Drupal\multi_crop\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\media\MediaInterface;
use Drupal\multi_crop\MultiCropPluginManager;
use Psr\Container\ContainerInterface;

/**
 * Plugin implementation of the 'image' formatter.
 *
 * @FieldFormatter(
 *   id = "image_multicrop",
 *   label = @Translation("Multi Crop Image"),
 *   field_types = {
 *     "image"
 *   },
 * )
 */
class MulticropFormatter extends ImageFormatter {

  /**
   * The multicrop plugin manager.
   *
   * @var \Drupal\multi_crop\MultiCropPluginManager
   */
  private $multiCropManager;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\multi_crop\MultiCropPluginManager $multiCropManager
   *   The multicrop plugin manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator = NULL, MultiCropPluginManager $multiCropManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->multiCropManager = $multiCropManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('plugin.manager.multi_crop')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $data = [];
    $crop_context_ref = NULL;
    $entity_parent = $items->getEntity() ?? NULL;

    // If parent is a media.
    if ($entity_parent != NULL && $entity_parent instanceof MediaInterface) {
      $crop_context_ref = $entity_parent->entity_reference_entity_modify ?? NULL;
      // If there is a referring entity.
      if ($referringItem = $entity_parent->_referringItem) {
        $media_override = $referringItem->getValue() ?? [];
        // If there is overwritten data.
        if (isset($media_override['overwritten_property_map'])) {
          $data = Json::decode($media_override['overwritten_property_map']);
        }
      }
    }

    if ($data != []) {
      foreach ($data as $field_name => $data_values) {
        // If there are overwritten data.
        if ($items->getName() == $field_name) {
          foreach ($elements as $key => $item) {

            if (isset($data_values[$key])) {

              $crop_settings = $this->getPluginSetting($data_values[$key]);
              // Send crop data to custom image formater.
              $elements[$key]['#multi_crop_attributes'] = $crop_settings;
              $elements[$key]['#multi_crop_attributes']['context'] = $crop_context_ref;
              $elements[$key]['#multi_crop_attributes']['image_style'] = $elements[$key]['#image_style'];
              $elements[$key]['#multi_crop_attributes']['base_crop_folder'] = 'multi_crop';
              $elements[$key]['#theme'] = 'multi_crop_image_formatter';
            }
          }
        }
      }
    }

    return $elements;
  }

  /**
   * Recover Crop settings.
   *
   * @param array $configuration
   *   Field information.
   *
   * @return array
   *   Crop settings.
   */
  private function getPluginSetting(array $configuration) {

    $plugin_definitions = $this->multiCropManager->getDefinitions();

    foreach ($plugin_definitions as $plugin_id => $plugin) {
      foreach ($configuration as $key => $value) {

        $target_field = $plugin['target_field_name'];
        if ($key === $target_field) {
          return [
            'plugin_id' => $plugin_id,
            'crop_setting' => $value,
          ];
        }
      }
    }
  }

}
