<?php

namespace Drupal\multi_crop;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for multi_crop plugins.
 */
abstract class MultiCropPluginBase extends PluginBase implements MultiCropInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetFieldName() {
    return (string) $this->pluginDefinition['target_field_name'];
  }

  /**
   * Create or load crop instance.
   *
   * @param string $crop_type
   *   Crop Type id.
   * @param string $old_uri
   *   Uri of original image.
   * @param string $new_uri
   *   Uri of the "copy image".
   *
   * @return \Drupal\crop\Entity\Crop
   *   Return the crop entity.
   */
  protected function retrievCrop($crop_type, $old_uri, $new_uri) {

    $cropStorage = \Drupal::service('entity_type.manager')->getStorage('crop');

    // Try to load crop for the current context.
    $base_crop = ['uri' => $new_uri, 'type' => $crop_type];
    $crop = $cropStorage->loadByProperties($base_crop);

    $crop = reset($crop) ?: NULL;

    // Try to load crop from source image.
    if ($crop == NULL) {

      /** @var \Drupal\file\FileInterface[] $files */
      $files = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['uri' => $old_uri]);
      /** @var \Drupal\file\FileInterface|null $file */
      $file = reset($files) ?: NULL;

      $values = [
        'type' => $crop_type,
        'entity_id' => $file->id(),
        'entity_type' => 'file',
        'uri' => $new_uri,
      ];

      // Create new cron.
      $crop = $cropStorage->create($values);
    }

    return $crop;
  }

  /**
   * {@inheritdoc}
   */
  public static function processFieldData($field_data) {
    return $field_data;
  }

}
