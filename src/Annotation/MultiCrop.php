<?php

namespace Drupal\multi_crop\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines multi_crop annotation object.
 *
 * @Annotation
 */
class MultiCrop extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The Target Field Name.
   *
   * @var string
   */
  public $target_field_name;

}
