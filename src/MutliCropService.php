<?php

namespace Drupal\multi_crop;

use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;

/**
 * Service description.
 */
class MutliCropService {

  /**
   * The plugin.manager.multi_crop service.
   *
   * @var \Drupal\multi_crop\MultiCropPluginManager
   */
  protected $multiCropManager;

  /**
   * Constructs a MutliCropService object.
   *
   * @param \Drupal\multi_crop\MultiCropPluginManager $multiCrop_manager
   *   The plugin.manager.multi_crop service.
   */
  public function __construct(MultiCropPluginManager $multiCrop_manager) {
    $this->multiCropManager = $multiCrop_manager;
  }

  /**
   * Undocumented function.
   *
   * @param array $old_image
   *   An image render array containing (at least):
   *   - #uri: the image uri
   *   - #width: the image width
   *   - #height: the image heigh.
   * @param array $settings
   *   The render array set in MulticropFormatter::viewElements with :
   *   - [Crop settings] (from the crop widget)
   *   - context : the media field identifier context
   *   - image_style : image style needed
   *   - base_crop_folder : folder to store cropped image.
   *
   * @return string
   *   The URI of cropped image.
   *
   *  @see \Drupal\multi_crop\Plugin\Field\FieldFormatter\MulticropFormatter::viewElements
   */
  public function prepareCropImage(array $old_image, array $settings) {

    $multi_crop_folder = $settings['base_crop_folder'] ?? NULL;

    // Copy source image.
    $new_uri = $this->multiCropTransformImageUri($old_image['#uri'], $settings, $multi_crop_folder);

    // Load dedicated multi crop plugin.
    $crop_type = $settings['plugin_id'];
    $plugin = $this->multiCropManager->createInstance($crop_type);

    // Create custom crop.
    $crop_created = $plugin->saveCrop(
      $settings['crop_setting'],
      $settings['image_style'],
      $old_image['#uri'],
      $new_uri,
      $old_image['#width'],
      $old_image['#height'],
    );

    return ($crop_created === TRUE) ? $new_uri : $old_image['#uri'];

  }

  /**
   * Copy source image to dedicated place.
   *
   * @param string $image_uri
   *   Source image URI.
   * @param array $crop_data
   *   Crop elements.
   * @param string $multi_crop_folder_name
   *   Name of multi_crop_folder in public paths.
   *
   * @return string
   *   New image source.
   */
  private function multiCropTransformImageUri($image_uri, array $crop_data, $multi_crop_folder_name = 'multi_crop') {

    $context = Html::cleanCssIdentifier($crop_data['context']);

    // Transform public://path/image
    // to public://style/[multi_crop_folder_name]/[context]/path/image.
    $uri_data = explode('//', $image_uri);
    $image_path = array_pop($uri_data);
    $path = explode('/', $image_path);
    $image_name = array_pop($path);
    $file_path = implode('/', $path);
    $new_path = $uri_data[0] . '//styles/' . $multi_crop_folder_name . '/' . $context . '/' . $file_path;
    $new_uri = $new_path . '/' . $image_name;

    // If image existe, delete it.
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $file_system->unlink($new_uri);

    // Use file_system services to create copy in targeted path.
    $filesystem = \Drupal::service('file_system');
    $filesystem->prepareDirectory($new_path, FileSystemInterface::CREATE_DIRECTORY);
    $filesystem->prepareDirectory($new_path, FileSystemInterface::MODIFY_PERMISSIONS);
    $filesystem->copy($image_uri, $new_uri, FileSystemInterface::EXISTS_REPLACE);

    // Calculate derivative URL, remove file if exist.
    $crop_type = $crop_data['image_style'];

    $derivative = $uri_data[0] . '//styles/' . $crop_type . '/public/styles/' . $multi_crop_folder_name . '/' . $context . '/' . $file_path . '/' . $image_name;
    if (file_exists($filesystem->realpath($derivative))) {
      unlink($filesystem->realpath($derivative));
    }
    return $new_uri;
  }

}
